AttributeNormalizer.configure do |config|
  config.normalizers[:sanitize] = lambda do |value, options|
    Sanitize.clean(value) if value.is_a?(String)
  end

  config.default_normalizers = :sanitize, :squish, :blank
end
