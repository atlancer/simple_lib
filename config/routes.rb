SimpleLib::Application.routes.draw do
  devise_for :users, :path_prefix => 'auth'

  resources :users, :only => :index
  resources :users, :only => :show do
    resources :books
  end

  resources :books, :only => [:index, :show]

  root :to => 'home#index'

  unless Rails.application.config.consider_all_requests_local
    match '*not_found', to: 'errors#error_404'
  end
end
