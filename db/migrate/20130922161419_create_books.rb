class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.belongs_to :user
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
