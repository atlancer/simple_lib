class AddBookfileToBooks < ActiveRecord::Migration
  def self.up
    add_attachment :books, :bookfile
  end

  def self.down
    remove_attachment :books, :bookfile
  end
end
