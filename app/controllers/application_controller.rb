class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!, except: [:index, :show]

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception,
                with: lambda { |exception| render_error 500, exception }

    rescue_from ActionController::RoutingError,
                ActionController::UnknownController,
                ::AbstractController::ActionNotFound,
                ActiveRecord::RecordNotFound,
                with: lambda { |exception| render_error 404, exception }
  end

  private

  def render_error(status, exception = nil)
    respond_to do |format|
      format.html { render template: "errors/error_#{status}", layout: 'layouts/error', status: status }
      format.all { render nothing: true, status: status }
    end
  end
end
