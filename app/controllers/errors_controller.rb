class ErrorsController < ApplicationController
  def error_404
    @not_found_path = params[:not_found]
    render_error(404)
  end

  def error_500
    render_error(500)
  end
end
