class HomeController < ApplicationController
  def index
    @recently_added_books = Book.recently_added
    @user_by_count_of_books = User.by_count_of_books
  end
end
