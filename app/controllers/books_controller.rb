class BooksController < ApplicationController
  before_filter :find_author, only: [:index, :show]

  def index
    @books = @author ? @author.books : Book.order(:title)
  end

  def show
    @book = @author ? @author.books.find(params[:id]) : Book.find(params[:id])
  end

  def new
    @book = current_user.books.new
  end

  def edit
    @book = current_user.books.find(params[:id])
  end

  def create
    @book = current_user.books.new(params[:book])

    if @book.save
      redirect_to [current_user, @book], notice: 'Book was successfully created.'
    else
      render action: "new"
    end
  end

  def update
    @book = current_user.books.find(params[:id])

    if @book.update_attributes(params[:book])
      redirect_to [current_user, @book], notice: 'Book was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @book = current_user.books.find(params[:id])
    @book.destroy

    redirect_to user_books_url(current_user)
  end

  private

  def find_author
    @author = params[:user_id] ? User.find(params[:user_id]) : nil
  end
end
