class Book < ActiveRecord::Base
  belongs_to :user, counter_cache: true
  has_attached_file :bookfile

  # attributes
  attr_accessible :title, :description, :bookfile
  normalize_attributes :title, :description

  # validation
  validates :title, presence: true, uniqueness: { scope: :user_id }
  validates_attachment_presence :bookfile
  validates_attachment_content_type :bookfile, content_type: ['application/pdf'], message: 'not pdf file'

  # scopes
  def self.recently_added count = 5
    unscoped.order(:created_at).reverse_order.limit(count)
  end
end
