class User < ActiveRecord::Base
  has_many :books
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # attributes
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name
  normalize_attributes :name

  # validation
  validates :name, presence: true

  # scopes
  TOP_BY_COUNT_OF_BOOKS = 3 # used in view
  scope :by_count_of_books, where("books_count > '0'").order('books_count desc').order(:name).limit(TOP_BY_COUNT_OF_BOOKS)
end
